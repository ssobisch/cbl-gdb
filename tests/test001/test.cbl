        >>SOURCE FREE
ID DIVISION.
PROGRAM-ID. test.
DATA DIVISION.
LOCAL-STORAGE SECTION.

01  GEORGE.
    02 FILLER     PIC  A(13) VALUE SPACES.
    02 NAME-FIRST PIC  A(10) VALUE "GEORGE".
01  WASHINGTON.
    02 FILLER     PIC  A(23) VALUE SPACES.
    02 NAME-FIRST PIC  A(10) VALUE "WASHINGTON".
    02 LOCATION   PIC  A(10) VALUE "DC".
    02 TEMPERATURE PIC A(10) VALUE "HOT".
    66  WABASH RENAMES NAME-FIRST OF WASHINGTON.
01  CARVER.
    02 FILLER     PIC  A(43) VALUE SPACES.
    02 NAME-FIRST PIC  A(10) VALUE "CARVER".
01  PEANUTS.
    02 FILLER     PIC  A(63) VALUE SPACES.
    02 NAME-FIRST PIC  999V999999 VALUE 123.456789.
01  LEGUMES REDEFINES PEANUTS.
    02 FILLER     PIC  A(63).
    02 NAME-FIRST PIC  999999V999.

PROCEDURE DIVISION.
    CALL 'JIMBOB'.
    DISPLAY NAME-FIRST of GEORGE.
    DISPLAY NAME-FIRST of WASHINGTON.
    DISPLAY NAME-FIRST of CARVER.
    DISPLAY NAME-FIRST of PEANUTS.
    DISPLAY NAME-FIRST of LEGUMES.
    MOVE "Wabash" TO WABASH.
    DISPLAY NAME-FIRST of WASHINGTON.

    END PROGRAM test.

IDENTIFICATION DIVISION.
PROGRAM-ID. JIMBOB.
DATA DIVISION.
WORKING-STORAGE SECTION.
   77 TEST-NUMBER-A             PIC 9(9) VALUE 123456789.
   77 TEST-NUMBER-B1            PIC 9999999V9 VALUE 1234567.8 .
   77 TEST-NUMBER-B2            REDEFINES TEST-NUMBER-B1 PIC 999999V99 .
   77 TEST-NUMBER-C             PIC 9(9) VALUE 123456789.
   77 errno                     BINARY-LONG VALUE 0.

   01 WS-JIM GLOBAL.
        02 WS-FIRSTNAME         PIC A(10)   VALUE "James".
        02 WS-MIDDLE-INITIAL    PIC X(1)    VALUE '"'.
        02 FILLER               PIC A(1)    VALUE SPACE.
        02 WS-LASTNAME          PIC A(10)   VALUE "Lowden".
   01 WS-BOB GLOBAL.
        02 WS-FIRSTNAME         PIC A(10)   .
        02 WS-MIDDLE-INITIAL    PIC A(1)    .
        02 FILLER               PIC A(1)    .
        02 WS-LASTNAME          PIC A(10)   .
   01 WS-JUDY.
   02 WS-FIRSTNAME PIC A(10) VALUE "Judy".
   02 WS-MIDDLE-INITIAL PIC A(1) VALUE "L".
   02 FILLER PIC A(1) VALUE SPACE.
   02 WS-LASTNAME PIC A(10) VALUE "Ruderman".

   01 UNUSED.
      02 NOT-USED PIC A(24).
   01 WS-FLOATTEST COMP-1 GLOBAL VALUE 1.234.
PROCEDURE DIVISION.
    MOVE    SPACES to WS-BOB.
    MOVE    "Robert" to WS-FIRSTNAME IN WS-BOB.
    MOVE    "J" to WS-MIDDLE-INITIAL IN WS-BOB.
    MOVE    "Dubner" to WS-LASTNAME IN WS-BOB.
    DISPLAY "entering: jimbob"
    DISPLAY TEST-NUMBER-B1.
    DISPLAY TEST-NUMBER-B2.
    CALL 'JIM'.
    CALL 'BOB'.
    CALL 'DISPLAY-JIM-OR-BOB' USING WS-JUDY,WS-JIM
    CALL 'JUST_ONE' USING 1031.
    DISPLAY WS-FLOATTEST.
    GOBACK.

IDENTIFICATION DIVISION.
PROGRAM-ID. JIM.
PROCEDURE DIVISION.
    DISPLAY "entering: jim"
    DISPLAY WS-JIM.
    DISPLAY "by pargraphs:"
    PERFORM ParaFirstName.
    PERFORM ParaMiddleInitial.
    PERFORM ParaLastName.
    DISPLAY "by threw"
    PERFORM ParaFirstName THRU ParaLastName.
    DISPLAY "by sekshun:"
    PERFORM FirstMiddleLast.
    GOBACK.
FirstMiddleLast SECTION.
    ParaFirstName.      DISPLAY WS-FIRSTNAME OF WS-JIM.
    ParaMiddleInitial.  DISPLAY WS-MIDDLE-INITIAL OF WS-JIM.
    ParaLastName.       DISPLAY WS-LASTNAME OF WS-JIM.
    END PROGRAM JIM.

IDENTIFICATION DIVISION.
PROGRAM-ID. DISPLAY-JIM-OR-BOB.
DATA DIVISION.
LINKAGE SECTION.
   01 LS-JORB.
        02 LS-FIRSTNAME         PIC A(10)   .
        02 LS-MIDDLE-INITIAL    PIC A(1)    .
        02 FILLER               PIC A(1)    .
        02 LS-LASTNAME          PIC A(10)   .
   01 LS-JORB-2.
        02 LS-FIRSTNAME         PIC A(10)   .
        02 LS-MIDDLE-INITIAL    PIC A(1)    .
        02 FILLER               PIC A(1)    .
        02 LS-LASTNAME          PIC A(10)   .
PROCEDURE DIVISION USING LS-JORB, LS-JORB-2.
    DISPLAY "entering: jorb"
*>    DISPLAY LS-JORB
    DISPLAY LS-MIDDLE-INITIAL OF LS-JORB.
*>    DISPLAY LS-JORB-2
    DISPLAY LS-MIDDLE-INITIAL OF LS-JORB-2.
    END PROGRAM DISPLAY-JIM-OR-BOB.

IDENTIFICATION DIVISION.
PROGRAM-ID. JUST_ONE.
DATA DIVISION.
LINKAGE SECTION.
   01 A_BINARY_NUMBER USAGE BINARY-LONG.
PROCEDURE DIVISION USING A_BINARY_NUMBER.
    DISPLAY "entering: JUST_ONE"
    DISPLAY A_BINARY_NUMBER.
    ADD 1 to A_BINARY_NUMBER.
    DISPLAY A_BINARY_NUMBER.
    ADD 1 to A_BINARY_NUMBER.
    DISPLAY A_BINARY_NUMBER.
    END PROGRAM JUST_ONE.

IDENTIFICATION DIVISION.
PROGRAM-ID. BOB.
PROCEDURE DIVISION.
    DISPLAY "entering: bob"
    DISPLAY WS-BOB.
    DISPLAY "by pargraphs:"
    PERFORM ParaFirstName.
    PERFORM ParaMiddleInitial.
    PERFORM ParaLastName.
    DISPLAY "by threw"
    PERFORM ParaFirstName THROUGH ParaLastName.
    DISPLAY "by sekshun:"
    PERFORM FirstMiddleLast.
    MOVE 4.321 TO WS-FLOATTEST
    GOBACK.
FirstMiddleLast SECTION.
    ParaFirstName.      DISPLAY WS-FIRSTNAME OF WS-BOB.
    ParaMiddleInitial.  DISPLAY WS-MIDDLE-INITIAL OF WS-BOB.
    ParaLastName.       DISPLAY WS-LASTNAME OF WS-BOB.
    END PROGRAM BOB.
END PROGRAM JIMBOB.






