        IDENTIFICATION DIVISION.
        PROGRAM-ID. A.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 CALL-BLOCK.
            05 WE-ARE-A PIC X(4) VALUE "A".
            05 WS-INDENT PIC 9.
        LINKAGE SECTION.
        COPY LINK.
        PROCEDURE DIVISION USING CALLED-BLOCK.
        ADD 2 TO INDENT GIVING WS-INDENT
        PERFORM INDENT TIMES
            DISPLAY ' ' WITH NO ADVANCING
            END-PERFORM
        DISPLAY "I am '" WE-ARE-A "', called from '" CALLED-FROM "'"
            WITH NO ADVANCING
        DISPLAY "; I call B and C"
        CALL "B" USING CALL-BLOCK
        CALL "C" USING CALL-BLOCK
        GOBACK.
