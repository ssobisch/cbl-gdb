// This file is used by subordinate projects to keep the versioning
// consistent through compilations and packaging.
//
// On the Unix side, there is scripting trickery to propogate this version
// code into cobcd.py and the names of packages when they are created.

#define VERSION "4.28.3"
